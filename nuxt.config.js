import path from "path";
import glob from "glob";

const [css, js] = glob.sync("**/*.{js,css}", {
  cwd: path.resolve(__dirname, "static"),
});

export default {
  ssr: false,

  server: {
    port: process.env.PORT,
    host: process.env.HOST,
  },

  generate: {
    dir: "public",
  },

  components: true,

  build: {
    babel: {
      plugins: [
        ['@babel/plugin-proposal-private-methods', { loose: true }],
        ["@babel/plugin-proposal-private-property-in-object", { "loose": true }]
      ]
    },
    extend(config, { }) {
      config.node = {
        fs: 'empty',
        child_process: 'empty'
      }
    }
  },

  head: {
    title: "DICT - Transaction Management System",

    meta: [
      { charset: "utf-8" },
      { "http-equiv": "X-UA-Compatible", content: "IE=edge,chrome=1" },
      {
        name: "viewport",
        content: "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no",
      },
      { name: "robots", content: "noindex,nofollow" },
    ],

    link: [
      { rel: "stylesheet", href: `/${css}` },
      { rel: "icon", type: "image/png", href: "/img/logo/dict-logo.png" },
    ],

    script: [
      { src: `/${js}`, body: true },
    ],
  },

  watchers: {
    hot: true,
    webpack: {
      aggregateTimeout: 300,
      poll: 1000,
    },
  },

  plugins: ['~/plugins/Validation', '~/plugins/Pagination', '~/plugins/Popover', '~/plugins/axios', '~/plugins/Api', '~/plugins/Helper'],

  modules: ["@nuxtjs/axios"],

  axios: {
    baseURL: `${process.env.API_BASE_URL}`,
    progress: false,
    // headers: {
    //   common: {
    //     Accept: "*/*",
    //     "Content-Type": "application/json",
    //   },
    // },
  },
  env: process.env
};
