const { serialize } = require("~/lib/Serializer");
const { to } = require("await-to-js");

const api_prefix = "/api/v1";
// const api_prefix = "http://localhost:8080/api/v1";

export const actions = {

	async getUserTickets(ctx, payload) {
	    const [err, data] = await to(
	        this.$axios.get(`${api_prefix}/helpdesk/users`, payload)
	    );
	    return serialize(err ? err.response : data);
	},
    async createUserTicket(ctx, payload) {
        const [err, data] = await to(
            this.$axios.post(`${api_prefix}/helpdesk/saveinfo`, payload)
        );
        return serialize(err ? err.response : data);
    },

    //my task
    async getUser (ctx, ticket_num) {
        const [err, data] = await to(
            this.$axios.get(`${api_prefix}/helpdesk/user/${ticket_num}`)
        );
        return serialize(err ? err.response : data);
    },

    async updateHelpdesk (ctx, payload) {
        const [err, data] = await to(
            this.$axios.post(`${api_prefix}/helpdesk/updateInfo`, payload)
        );
        return serialize(err ? err.response : data);
    },


    async saveAllSchedule (ctx, payload) {
        const [err, data] = await to(
            this.$axios.post(`${api_prefix}/schedAct/saveSchedule`, payload)
        );
        return serialize(err ? err.response : data);
    },

    async findAllSchedule (ctx) {
        const [err, data] = await to(
            this.$axios.get(`${api_prefix}/schedAct/schedules`)
        );
        return serialize(err ? err.response : data);
    },

    async getThisUserTickets (ctx,payload) {
        const [err, data] = await to(
            this.$axios.get(`${api_prefix}/helpdesk/userTickets/${payload}`)
        );
        return serialize(err ? err.response : data);
    },

    async getIbsMalwareSla (ctx) {
        const [err, data] = await to(
            this.$axios.get(`${api_prefix}/sla/HOA`)
        );
        return serialize(err ? err.response : data);
    },
    
};