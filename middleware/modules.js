const checkUrlName = (path="", name="") => {
  return path?.toLowerCase()?.includes(name.toLowerCase()) || false;
}

export default function ({ store, redirect, params, route }) {
  let session = JSON.parse(localStorage.getItem('user') || "{}");
  // console.log('session middleware', session)
  // console.log('session.userType', session.userType)
  let session_module = session.module || [];

  if (session?.userClass?.toLowerCase() || "" === 'admin') {
    session_module.push('SDCC');
  }

  // const checkUser = () => {
  //   let user = JSON.parse(localStorage.getItem("user") || "{}");
  //   let userInfo = JSON.parse(localStorage.getItem('userComp')||"{}");
  //   if (user.userType==='NGA') {
  //     let isCaap = this.userInfo.agencyName.toLowerCase().includes('caap');
  //     let isDpwh = this.userInfo.agencyName.toLowerCase().includes('dpwh');
  //     return isCaap?'CAAP':(isDpwh?'DPWH':'');
  //   }
  //   else {
  //     return user.userType;
  //   }
  // }


  let module_list = {
    "ebpls": "EBPLS",
    "ictlc": "ICTLC",
    "datacenter": "DCE",
    "dm": "DM",
    "dns": "DNS",
    "gwhs": "GWHS",
    "govmail": "GovMail",
    "digitalcertificates": "DC",
    "govnet": "GovNet",
    "sharedptti": "SP",
    "pttiapplication": "PRA",
    "ibsapplication": "IBS",
    "focapplication": "FRA",
    "pttiapplication": "PTTI",
    "focapplication": "FOC",
    "extractreport": "ER",
    "templatedreport": "TR",
    "emailreport": "EMR",
    "holidays": "sdcc",
    "sla": "sla",
    "sdcc-holidays-sla": "SDCC",
    "wfm-taskflow": "WFMTA",
  }
  let routted = typeof route.name === 'string' ? route.name : '';
  let splitted = routted.split('-');
  let module = splitted[1] || (splitted[0] || "");
  let key = module.toLowerCase();

  if (splitted?.[0]?.toLowerCase() === 'ictlc') {
    key = 'ictlc'
  }

  let isSdcc = checkUrlName(splitted?.[0]||"",'sdcc');
  let isHolidays = checkUrlName(splitted?.[1]||"",'holidays');
  let isSla = checkUrlName(splitted?.[1]||"",'sla');
  if (isSdcc && (isHolidays || isSla)) {
    key = 'sdcc-holidays-sla'
  }

  let isWfm = checkUrlName(splitted?.[0]||"",'workflowmanagement');
  let isTaskflow = checkUrlName(splitted?.[1]||"",'taskflow');
  if (isWfm && (isTaskflow)) {
    key = 'wfm-taskflow'
  }

  let exeptions = [
    "Dashboard-SeeMore",
    "Tracker",
    "Account-Profile",
    "Account-UpdateProfile",
    "Account-CreateProfile",
    "Account-UAMDashboard",
  ];

  // session_module.push("HD");

  // if (['LGU'].indexOf(checkUser)>-1) {
  //   session_module = session_module.filter(function(item) {
  //     return item !== "IBS"
  //   });
  // }

  // if (['CAAP'].indexOf(checkUser)>-1) {
  //   session_module = session_module.filter(function(item) {
  //     return item == "PTTI"
  //   });
  // }

  // if (['DPWH'].indexOf(checkUser)>-1) {
  //   session_module = session_module.filter(function(item) {
  //     return item == "FOC"
  //   });
  // }

  // if (session_module.indexOf('HDA') > -1) {
  //   session_module = ["HDA"];
  // }

  // if (store.state.session.userType.toLowerCase()==="uam admin") {
  //   session_module = session_module.filter(function(item) {
  //     return item !== "HD" && item !== 'HDA';
  //   });
  //   session_module.push("UAM");
  // }

  // if (store.state.session.userType.toLowerCase()==="telco" || store.state.session.userType.toLowerCase()==="itc") {
  //   session_module.push("ICTLCT");
  //   session_module.push("ICTLC");
  //   session_module.push("DCE");
  // }

  // Temporary - Citizens Charter access
  // if (store.state.session.userType.toLowerCase()==="" || store.state.session.userType.toLowerCase()!=="") {
  if (true) {
    session_module.push("ICTLC");
    session_module.push("DCE");
    session_module.push("DC");
    session_module.push("GovNet");
    session_module.push("GWHS");
    session_module.push("EBPLS");
    session_module.push("GovMail");
    // if(store.state.session.userClass==="GR"){
    //   session_module.push("GovNet");
    // } 

    // reports generation access
    session_module.push("ER");
    session_module.push("TR");
    session_module.push("EMR");
  }

  if (exeptions.indexOf(routted) > -1) return;

  // if (store.state.session.userType==="OSC") {
  //   session_module = ['PTTI', 'FOC', 'IBS', 'EBPLS', 'ICTLC', 'DC', 'DNS', 'GWHS', 'GovMail', 'DCE', 'GovNet', 'HD'];
  // }

  // if (key!=="dashboard" && session_module.indexOf(module_list[key]) === -1 && store.state.session.userClass !== 'Admin') {
  // if (key!=="dashboard" && session_module.indexOf(module_list[key]) === -1 && (['Admin', 'OSC'].indexOf(store.state.session.userClass) === -1 || ['UAM Admin', 'OSC'].indexOf(store.state.session.userType) === -1 )) {
  if (key !== "dashboard" && session_module.indexOf(module_list[key]) === -1 && (['Admin'].indexOf(store.state.session.userClass) === -1 || ['UAM Admin'].indexOf(store.state.session.userType) === -1)) {
    if (session_module.indexOf("UAMA") > -1) {
      const hdaAllowed = {
        "UAMA": [
          "list",
          "updateaccesslist",
          "create",
          "view",
          "update",
        ],
        "UAM": [
          "list",
          "updateaccesslist",
          "create",
          "view",
          "update",
        ]
      }
      const activeModule = session_module.includes("UAMA") ? "UAMA" : (session_module.includes("UAM") ? "UAM" : "");
      if (hdaAllowed[activeModule].includes(key)) {
        return;
      }
    }
    if (splitted.map(item => item.toLowerCase()).indexOf("helpdesk") > -1 && (session_module.indexOf("HD") > -1 || session_module.indexOf("HDA") > -1)) {
      const hdaAllowed = {
        "HDA": [
          "list",
          "selfdiagnostic",
          "scheduleactivities"
        ],
        "HD": [
          "list",
          "selfdiagnostic",
          "usertickets"
        ]
      }
      const activeModule = session_module.includes("HDA") ? "HDA" : (session_module.includes("HD") ? "HD" : "");
      if (hdaAllowed[activeModule].includes(key)) {
        return;
      }
    }
    return redirect('/errors/404');
  }
}