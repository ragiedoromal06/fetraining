export default function ({ $axios }, inject) {
    // Create a custom axios instance
    const api = $axios.create({
      headers: {
        common: {
          Accept: 'application/json, text/plain, */*',
          "Content-Type": 'application/json;charset=UTF-8'
        }
      }
    })
    
    // Set baseURL to something different
    api.setBaseURL(`http://159.138.24.196:2020`)
  
    // Inject to context as $api
    inject('api', api)
    // ctx.$api = api
  }