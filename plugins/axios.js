export default function ({ $axios, store }) {
  $axios.interceptors.request.use(config => {
    const except_url = [
      "/auth/login",
      "/auth/forgotpassword",
      "/auth/register",
      "/auth/changePassword"
    ];
    if (localStorage.getItem("_token") && except_url.indexOf(config.url) === -1) {
      config.headers.Authorization = `Bearer ${localStorage.getItem("_token")}`;
    }
    
    return config;
  });

  $axios.interceptors.response.use(config => {
    let token = config.headers.authorization;
    return config;
  }, function (error) {
    return Promise.reject(error);
  });
}