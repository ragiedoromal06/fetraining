// const toBase64 = (fileObject) => {
//   const reader = new FileReader();
//   reader.onload = (e) => {
//     return e.target.result;
//   }
//   reader.readAsBinaryString(fileObject);
// };

const toBase64 = fileObject => new Promise((resolve, reject) => {
  const reader = new FileReader();
  reader.readAsDataURL(fileObject);
  reader.onload = () => resolve(reader.result);
  reader.onerror = error => reject(error);
});

export default (ctx, inject) => {
  const helper = {
    toBase64,
  };

  inject("helper", helper);

  ctx.$helper = helper;
};
