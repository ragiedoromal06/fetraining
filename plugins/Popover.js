const icons = {
  plus: "fas fa-plus",
  question: "far fa-question-circle",
  success: "far fa-check-circle",
  fail: "far fa-times-circle",
  warning: "fas fa-exclamation-triangle",
  loading: "fas fa-circle-notch fa-spin",
};

const getIcon = (type) => {
  return icons[type];
};



const show = (_options={}, iconType="question", type="green", bgOpacity=0, scrollToPreviousElement=true, scrollToPreviousElementAnimate=true) => {
  const defaultOptions = {
    scrollToPreviousElement: scrollToPreviousElement,
    scrollToPreviousElementAnimate: scrollToPreviousElementAnimate,
    title: "title",
    content: "content",
    type: type,
    theme: "material",
    icon: getIcon(iconType),
    backgroundDismissAnimation: "none",
    columnClass: "col-lg-6",
    animationSpeed: 500,
    bgOpacity: bgOpacity,
    buttons: {
      close: {
        text: "close",
        btnClass: "btn-danger",
        keys: ["enter"],
      },
    },
  };

  const options = {
    ...defaultOptions,
    ..._options,
  };

  $.alert(options);
};





const confirm = ({
  title='Title', text='Text', showCancelButton=true,
  confirmButtonClass="btn btn-info btn-fill", confirmButtonText="Continue",
  cancelButtonClass="btn btn-danger btn-fill", cancelButtonText="Cancel",
  closeOnConfirm=true,
  onContinue, onCancel,
  showIcon=true
}) => {
  swal({
    title: title,
    text: text,
    type: showIcon ? "warning" : "",
    showCancelButton: showCancelButton,
    confirmButtonClass: confirmButtonClass,
    confirmButtonText: confirmButtonText,
    cancelButtonClass: cancelButtonClass,
    cancelButtonText: cancelButtonText,
    closeOnConfirm: closeOnConfirm,
    allowOutsideClick: false,
  }, function(state) {
    if (state) {
      typeof onContinue==='function' ? onContinue() : '';
    }
    else {
      typeof onCancel==='function' ? onCancel() : '';
    }
  })
};

const html = ({
  title='Title', text='Text', showCancelButton=false, showConfirmButton=false,
  confirmButtonClass="btn btn-info btn-fill", confirmButtonText="Continue",
  cancelButtonClass="btn btn-danger btn-fill", cancelButtonText="Cancel",
  closeOnConfirm=true, hasInput=false, placeholder="",
  onContinue, onCancel, onConfirm,
  html=[]
}) => {
  const createButton = (cls="primary", text, cb, cls2='') => {
    return $(`<button class="btn btn-${cls} ${cls2}">${text}</button>`).on('click', cb);
  }

  let div = $('<div>');

  if (html.length > 0) {
    html.map((item) => {
      if (item.type == 'html') {
        div.append(item.content);
      }
      else if (item.type == 'button') {
        div.append(createButton(item.color, item.label, function() {
          typeof item.onClick==='function' ? item.onClick() : '';
        }))
      }
    })
  }

  if (hasInput) {
    div.append(`
      <input class="form-control" placeholder="${placeholder}" id="input-field-value" />
    `);
  }
  
  swal({
    title: title,
    html: div,
    showConfirmButton: showConfirmButton,
    showCancelButton: showCancelButton,
    confirmButtonClass: confirmButtonClass,
    confirmButtonText: confirmButtonText,
    cancelButtonClass: cancelButtonClass,
    cancelButtonText: cancelButtonText,
    closeOnConfirm: closeOnConfirm,
    onContinue, onCancel,
    allowOutsideClick: false,
  }, function(state) {
    if (state) {
      if (hasInput) {
        let input = $('#input-field-value').val();
        typeof onConfirm==='function' ? onConfirm(input) : '';
        return;
      }
      typeof onContinue==='function' ? onContinue() : '';
    }
    else {
      typeof onCancel==='function' ? onCancel() : '';
    }
  })
}


const warning = ({
  title='Title', text='Text',
  onContinue, onCancel,
  contentType='default',
  content=[],
  showIcon=false,
}) => {
  if (contentType=='default') {
    let hasIcon = {}
    showIcon?(hasIcon.type="warning"):{};
    swal({
      title: title,
      text: text,
      ...hasIcon,
      showCancelButton: true,
      cancelButtonClass: "btn btn-danger btn-fill",
      cancelButtonText: "Close",
      showConfirmButton: false,
      allowOutsideClick: false,
    }, function(state) {
      if (state) {
        typeof onContinue==='function' ? onContinue() : '';
      }
      else {
        typeof onCancel==='function' ? onCancel() : '';
      }
    })
  }
  else if (contentType=='html') {
    html({
      title:title, 
      showCancelButton: true,
      cancelButtonText:"Close",
      onContinue, onCancel,
      html:content,
      onCancel:() => {
        typeof onContinue==='function' ? onCancel() : '';
      }
    });
  }
};


const alert = ({
  title='', text='Text', showCancelButton=false, showConfirmButton=true,
  confirmButtonClass="btn btn-info btn-fill", confirmButtonText="OK",
  cancelButtonClass="btn btn-danger btn-fill", cancelButtonText="No",
  closeOnConfirm=true,
  onOK, onCancel,
}) => {
  let div = $('<div>');

  div.append(`
  <div class="row" style="
    border: 1px dotted grey;
    margin: 10px 10px 20px 10px;
    padding: 15px 10px 0 10px;
  ">
    <div class="col-lg-12">
      <p style="font-size: 15px;margin-bottom: 15px;">
        ${text}
      </p>
    </div>
  </div>
  `);
  
  swal({
    title: title,
    html: div,
    showConfirmButton: showConfirmButton,
    showCancelButton: showCancelButton,
    confirmButtonClass: confirmButtonClass,
    confirmButtonText: confirmButtonText,
    cancelButtonClass: cancelButtonClass,
    cancelButtonText: cancelButtonText,
    closeOnConfirm: closeOnConfirm,
    onOK, onCancel,
    allowOutsideClick: false,
  }, function(state) {
    if (state) {
      typeof onOK==='function' ? onOK() : '';
    }
    else {
      typeof onCancel==='function' ? onCancel() : '';
    }
  })
}

const yesNo = ({
  title='', text='Text', showCancelButton=true, showConfirmButton=true,
  confirmButtonClass="btn btn-info btn-fill", confirmButtonText="Yes",
  cancelButtonClass="btn btn-danger btn-fill", cancelButtonText="No",
  closeOnConfirm=true,
  onYes, onNo
}) => {
  let div = $('<div>');

  div.append(`
  <div class="row" style="
    border: 1px dotted grey;
    margin: 10px 10px 20px 10px;
    padding: 15px 10px 0 10px;
  ">
    <div class="col-lg-12">
      <p style="font-size: 15px;margin-bottom: 15px;">
        ${text}
      </p>
    </div>
  </div>
  `);
  
  swal({
    title: title,
    html: div,
    showConfirmButton: showConfirmButton,
    showCancelButton: showCancelButton,
    confirmButtonClass: confirmButtonClass,
    confirmButtonText: confirmButtonText,
    cancelButtonClass: cancelButtonClass,
    cancelButtonText: cancelButtonText,
    closeOnConfirm: closeOnConfirm,
    onYes, onNo,
    allowOutsideClick: false,
  }, function(state) {
    if (state) {
      typeof onYes==='function' ? onYes() : '';
    }
    else {
      typeof onNo==='function' ? onNo() : '';
    }
  })
}


export default (ctx, inject) => {
  const popoverFn = {
    show,
    getIcon,
    warning,
    confirm,
    html,
    yesNo,
    alert,
  };

  inject("popover", popoverFn);

  ctx.$popover = popoverFn;
};
