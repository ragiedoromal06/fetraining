const path = require("path");
const MergeIntoSingleFilePlugin = require("webpack-merge-and-include-globally");
const { minify } = require("terser");
const CleanCSS = require("clean-css");
const RemovePlugin = require("remove-files-webpack-plugin");
const webpack = require('webpack');

const scripts = [
  "assets/js/core/jquery.3.2.1.min.js",
  "assets/js/jquery-ui-1.12.1/jquery-ui.min.js",
  "assets/js/core/popper.min.js",
  "assets/js/core/bootstrap.min.js",
  "assets/js/plugins/jquery.validate.min.js",
  "assets/js/bootstrap-select.min.js",
  "assets/js/plugins/bootstrap-switch.js",
  "assets/js/plugins/chartist.min.js", 
  "assets/js/plugins/chartist-hover.js", 
  "assets/js/plugins/bootstrap-notify.js",
  "assets/js/plugins/jquery-jvectormap.js",
  "assets/js/plugins/moment.min.js",
  "assets/js/plugins/bootstrap-datetimepicker.js",
  "assets/js/plugins/sweetalert2.min.js",
  "assets/js/plugins/bootstrap-tagsinput.js",
  "assets/js/plugins/nouislider.js",
  "assets/js/plugins/jquery.bootstrap-wizard.js",
  "assets/js/plugins/bootstrap-table.js",
  "assets/js/plugins/jquery.dataTables.min.js",
  "assets/js/plugins/fullcalendar.min.js",
  "assets/js/light-bootstrap-dashboard.js",
  "assets/pincode-input/pincode-input.min.js",
  "assets/js/jquery.dataTables.min.js",

];

const styles = [
  "assets/css/bootstrap.min.css",
  "assets/css/bootstrap-select.min.css",
  "assets/css/light-bootstrap-dashboard.css",
  "assets/pincode-input/pincode-input.min.css",
  "assets/css/demo.css",
  "assets/css/style.css",
  "assets/css/jquery.dataTables.min.css",
];

const staticPath = path.resolve(__dirname, "static");

module.exports = {
  entry: "./package.json",
  output: {
    filename: "[name]",
    path: staticPath,
  },
  plugins: [
    new MergeIntoSingleFilePlugin({
      files: {
        "js/bundle.js": scripts.map((script) => {
          return path.resolve(__dirname, script);
        }),
        "css/assets.css": styles.map((style) => {
          return path.resolve(__dirname, style);
        }),
      },
      transform: {
        "js/bundle.js": async (code) => {
          const mini = await minify(code);

          return mini.code;
        },
        "css/assets.css": async (style) =>
          new CleanCSS({}).minify(style).styles,
      },
      transformFileName: (filename, extension, hash) => {
        const folder = filename.split("/")[0];

        return `${folder}/${hash}${extension}`;
      },
    }),
    new RemovePlugin({
      before: {
        root: staticPath,
        include: ["js", "css"],
      },
      after: {
        root: staticPath,
        include: ["main"],
      },
    }),
  ],
};
